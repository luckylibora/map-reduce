const fs = require('fs');
const readline = require('readline');
const util = require('util');

const fsClose = util.promisify(fs.close);
const fsOpenAsync = util.promisify(fs.open);
const fsRead = util.promisify(fs.read);
const fsStat = util.promisify(fs.stat);

const bufferSize = 2 * 1024 * 1024;

class DataFile {
  constructor(filePath) {
    this.filePath = filePath;
    this._bytesRead = 0;
    this._fd = null
    this._lineIndex = 0;
    this._linesBuffer = [];
    this._line = null;
    this._size = null;
  }

  async close() {
    this._linesBuffer = null;
    return await fsClose(this._fd);
  }

  async hasNext() {
    if (!this._linesBuffer.length) {
      await this._read();
    }
    return this._linesBuffer.length > 0;
  }

  async readNextLine() {
    if (!this._linesBuffer.length) {
      await this._read();
    }
    this._lineIndex++;
    return this._linesBuffer.pop();
  }

  async readNLinesWithOffset({offset, limit}) {
    const isFinished = await this.setLineOffset(offset);
    if (isFinished) {
      return [];
    }

    const lines = [];
    for (let i = 0; i < limit; i++) {
      const hasNext = await this.hasNext();
      if (!hasNext) {
        break;
      }
      const line = await this.readNextLine();
      lines.push(line);
    }
    return lines;
  }

  async setLineOffset(n) {
    while (this._lineIndex < n) {
      const hasNext = await this.hasNext();
      if (!hasNext) {
        return true;
      }
      await this.readNextLine();
    }
    return false;
  }

  async _read() {
    if (!this._fd) {
      this._fd = await fsOpenAsync(this.filePath, 'r+');
    }

    if (this._size === null) {
      this._size = (await fsStat(this.filePath)).size;
    }

    if (this._bytesRead >= this._size) {
      return;
    }

    const buffer = new Buffer.alloc(bufferSize);
    const {bytesRead} = await fsRead(this._fd, buffer, 0, buffer.length, null);
    if (bytesRead === 0) {
      return;
    }

    this._bytesRead += bytesRead;
    const s = buffer.slice(0, bytesRead).toString();
    let temp = s.split('\n');
    if (!temp.length) {
      return
    }

    if (this._line) {
      temp[0] = this._line + temp[0];
    }
    temp.reverse();
    this._line = temp[0];
    this._linesBuffer.push(...temp.slice(1));
  }
}

module.exports = {DataFile};