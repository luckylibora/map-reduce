const cluster = require('cluster');
const os = require('os');
const {Reducer} = require("./reducer");
const {Mapper} = require("./mapper");

function worker() {
  const mapper = new Mapper();
  process.on('message', async ({offset, limit}) => {
    const res = await mapper.map({offset, limit})
    process.send(res);
  });
}

function master() {
  const limit = 1.5 * 1024 * 1024;
  const workersNum = os.cpus().length;
  const reducer = new Reducer();
  let offset = null;
  let res = {};
  let completed = 0;

  function getNextTask() {
    if (offset === null) {
      offset = 0;
    } else {
      offset += limit;
    }
    return {offset, limit};
  }

  function handleResponse(worker, message) {
    if (message !== null) {
      res = reducer.reduce(res, message);
      worker.send(getNextTask());
    } else {
      completed++;
      worker.kill();
      if (completed === workersNum) {
        console.log(res);
      }
    }
  }

  for (let i = 0; i < workersNum; i++) {
    const worker = cluster.fork();
    worker.on('message', (message) => handleResponse(worker, message));
    worker.send(getNextTask());
  }
}

if (cluster.isMaster) {
  master();
} else {
  worker();
}