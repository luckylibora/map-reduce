function parseDonation(line) {
  const [projectId, donationId, donorId, donationIncluded, donationAmount] = line.split(',', 5);
  return {donorId, donationAmount};
}

function parseDonor(line) {
  const [donorId, donorCity, donorState] = line.split(',', 3);
  return {donorId, donorState};
}

module.exports = {parseDonation, parseDonor};