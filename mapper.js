const {parseDonation, parseDonor} = require('./parsers');
const {DataFile} = require('./data.file');

const donationsDataFilePath = './data/Donations.csv';
const donorsDataFilePath = './data/Donors.csv';

class Mapper {
  constructor() {
    this._donationsDataFile = new DataFile(donationsDataFilePath);
  }

  async map({offset, limit}) {
    const lines = await this._donationsDataFile.readNLinesWithOffset({offset, limit});
    if (!lines.length) {
      return null;
    }
    const groupedDonations = await this._groupDonations(lines);
    const joined = await this._joinDonors(groupedDonations);
    return this._groupByState(joined);
  }

  async _groupDonations(lines) {
    const reduced = lines.reduce((prev, curr) => {
      const {donationAmount, donorId} = parseDonation(curr);
      if (!donationAmount.length) {
        return prev;
      }

      const amount = parseFloat(donationAmount);
      if (!amount) {
        return prev;
      }

      prev[donorId] = (prev[donorId] || 0) + amount;
      return prev;
    }, {});
    return Object.entries(reduced);
  }

  async _joinDonors(records) {
    if (!records.length) {
      return [];
    }

    records.sort(([key1], [key2]) => {
      if (key1 > key2) {
        return -1;
      } else if (key1 < key2) {
        return 1;
      }
      return 0;
    });

    const donorsDataFile = new DataFile(donorsDataFilePath);
    const res = [];
    let [donorId, amount] = records.pop();
    do {
      const currentDonor = parseDonor(await donorsDataFile.readNextLine());
      if (donorId === currentDonor.donorId) {
        res.push([currentDonor.donorState, amount]);
        if (records.length) {
          [donorId, amount] = records.pop();
        } else {
          donorId = null;
        }
      }
    } while (records.length && donorId !== null && (await donorsDataFile.hasNext()))
    await donorsDataFile.close();
    return res;
  }

  _groupByState(records) {
    const reduced = records.reduce((prev, [state, amount]) => {
      prev[state] = (prev[state] || 0) + amount;
      return prev;
    }, {})
    return Object.entries(reduced);
  }
}

module.exports = {Mapper};