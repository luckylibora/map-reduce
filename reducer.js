class Reducer {
  reduce(prev, curr) {
    curr.forEach(([key, value]) => {
      prev[key] = (prev[key] || 0) + value
    });
    return prev;
  }
}

module.exports = {Reducer};